

#define _CRT_SECURE_NO_WARNINGS

#include "tinyxml2.h"

#include <iostream>
#include <regex>
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>

#include <cstring>
#include <cstdio>

using namespace std;
using namespace tinyxml2;
// took a break at 9:01 pm - 2 hours and 30 mins left
// resumed at 9:32 pm

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#ifndef XMLCheckResult
#define XMLCheckResult(a_eResult) if (a_eResult != XML_SUCCESS) { printf("Error: %i\n", a_eResult); }
#endif
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class NotebookEntry
{
    public:
    std::string name;
    std::string number;
    std::string street;
    std::string zipcode;

    NotebookEntry() : name( "" ), number( "" ), street( "" ), zipcode( "" ) {}
    virtual ~NotebookEntry() {}
    bool someFieldsAreEmpty()
    {

        return ( name.empty() || number.empty() || street.empty() || zipcode.empty() );
    }

    void Print()
    {
        std::cout << "name = " << name << " - number = " << number << " - street = " << street << " - zipcode = " << zipcode << std::endl;
    }
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
std::vector< NotebookEntry > parseProperAddressBook( string& filename )
{
    std::vector< NotebookEntry > addressBook;
    FILE* f = fopen( filename.c_str(), "r" );
    if( !f ) {
        std::cout << "Error parsing address book for reading. Aborting.\n";
        return addressBook;
    }
    const int buffer_size = 2048;
    const int huge_buffer_size = buffer_size * 10;
    char buffer[ buffer_size ];

    std::string content;
    while( !feof( f ) )
    {
        bool name_found = false;
        bool number_found = false;
        bool street_found = false;
        bool zipcode_found = false;
        memset( buffer, 0, buffer_size );
        fgets( buffer, buffer_size, f );
        std::string str( buffer );
        str.erase(std::remove(str.begin(), str.end(), '\r'),str.end());
        str.erase(std::remove(str.begin(), str.end(), '\n'),str.end());
        content.append( str );
    }
    std::string s = content;
    

    size_t pos = 0;
    std::string token;
    int counter = 0;
    std::string delimiter_name      = "NAME";
    std::string delimiter_number    = "NUMBER";
    std::string delimiter_street    = "STREET";
    std::string delimiter_zipcode   = "ZIPCODE";
    while ((pos = s.find(delimiter_name)) != std::string::npos) {
        token = s.substr(0, pos);
        counter++;
        NotebookEntry entry;
        s.erase(0, pos + delimiter_name.length());
        if ((pos = s.find(delimiter_number ) ) != std::string::npos ) {
            token = s.substr(0, pos);
            s.erase(0, pos + delimiter_number.length());
            entry.name = token;
        }
        if ((pos = s.find(delimiter_street ) ) != std::string::npos ) {
            token = s.substr(0, pos);
            s.erase(0, pos + delimiter_street.length());
            entry.number = token;
        }

        
        if ((pos = s.find(delimiter_zipcode ) ) != std::string::npos ) {
            token = s.substr(0, pos);
            s.erase(0, pos + delimiter_zipcode.length());
            entry.street = token;
        }

        std::string s1 = s;
        if ((pos = s1.find(delimiter_name ) ) != std::string::npos ) {
            token = s1.substr(0, pos);
            s1.erase(0, pos + delimiter_name.length());
            entry.zipcode = token;
        } else {
            entry.zipcode = s;
        }

        if( !entry.someFieldsAreEmpty() )
        {
            addressBook.push_back( entry );
        }
    }
    fclose( f );

    return addressBook;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void dirtyQueryParser()
{
    XMLDocument doc;
    const char* path = "input_xml_example.xml";
    char amountText[100];
    // Load the XML file into the Doc instance
    XMLError eResult = doc.LoadFile(path);
    XMLCheckResult(eResult);

    // Get root Element
    XMLElement* pRootElement = doc.RootElement();
    std::cout << "Printing XML stuff" << std::endl;
    std::cout << "[" << pRootElement->Name() << "]"  << std::endl;
    XMLElement* pTop = doc.RootElement();
    std::cout << pTop->GetText() << std::endl;
    return;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void LegacyCode(void)
{
    string str1 = "data_example_1.txt";
    string str2 = "data_example_2.txt";
    string str3 = "input_xml_example.xml";

    std::vector< string > filenames;
    filenames.push_back(str1);
    filenames.push_back(str2);
    filenames.push_back(str3);

    std::cout << "MAIN begins" << std::endl;
    std::vector< NotebookEntry > addressBook = parseProperAddressBook(filenames[0]);
    for (int i = 0; i < addressBook.size(); i++) {
        addressBook[i].Print();
    }

    // running query
    for (int i = 0; i < addressBook.size(); i++) {
        if (addressBook[i].name == string("Dave Smith")) {
            std::cout << "Entry index #" << i << " has the corresponding NAME = Dave Smith" << std::endl;
            std::cout << "details:" << std::endl;
            addressBook[i].Print();
            std::cout << std::endl;
        }
        if (addressBook[i].number == string("1234")) {
            std::cout << "Entry index #" << i << " has the corresponding NUMBER = 1234" << std::endl;
            std::cout << "details:" << std::endl;
            addressBook[i].Print();
            std::cout << std::endl;
        }
        if (addressBook[i].street == string("PINE")) {
            std::cout << "Entry index #" << i << " has the corresponding STREET = PINE" << std::endl;
            std::cout << "details:" << std::endl;
            addressBook[i].Print();
            std::cout << std::endl;
        }
        if (addressBook[i].zipcode == string("72111")) {
            std::cout << "Entry index #" << i << " has the corresponding ZIPCODE = 72111" << std::endl;
            std::cout << "details:" << std::endl;
            addressBook[i].Print();
            std::cout << std::endl;
        }
    }
    return;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
int main( int argc, char* argv[  ] )
{
    //LegacyCode();
    dirtyQueryParser();
    return 0;
}